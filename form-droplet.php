<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Droplet Create</title>
</head>

<body>
    <form action="database/post.php" method="POST">
        <label for="name">Droplet
            <input name="droplet" type="text" id="name">
        </label>
        <label for="region">Region</label>
        <select name="region" id="region">
            <option value="NYC1">NYC1</option>
            <option value="NYC2">NYC2</option>
            <option value="NYC3">NYC3</option>
            <option value="AMS2">AMS2</option>
            <option value="AMS3">AMS3</option>
            <option value="SFO1">SFO1</option>
            <option value="SFO2">SFO2</option>
            <option value="SGP1">SGP1</option>
            <option value="LON1">LON1</option>
            <option value="FRA1">FRA1</option>
            <option value="TOR1">TOR1</option>
            <option value="BLR1">BLR1</option>
        </select>
        <label for="#image">Imagen</label>
        <input type="file" name="image" id="image">

            <input type="submit" name="salve" value="Create" class="buttom">
    </form>
</body>

</html>