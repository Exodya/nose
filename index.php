<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Droplets</title>
</head>

<body>
<h1>Droplets</h1>
    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Droplet</th>
                <th>Region</th>
                <th>image</th>
                <th>id_droplet</th>
            </tr>
        </thead>

        <?php require('database/table.php'); ?>
        <?php foreach ($tasks as $task) : ?>
            <tbody>
                <tr>
                    <td><?= htmlspecialchars($task->id) ?></td>
                    <td><?= htmlspecialchars($task->droplet) ?></td>
                    <td><?= htmlspecialchars($task->region) ?></td>
                    <td><?= htmlspecialchars($task->image) ?></td>
                    <td><?= htmlspecialchars($task->id_droplet) ?></td>
                    <td>
                        <form action="droplet/destroy.php" method="POST">
                            <input type="hidden" class="delete" name="id" value=<?= " $task->id " ?>>
                            <button type="submit">Delete</button>
                        </form>
                    </td>
                </tr>
            </tbody>
        <?php endforeach; ?>
    </table>
    <a href="form-droplet.php">Crear Droplet</a>
</body>

</html>