<?php
session_start();
$_SESSION['message'] = "";

require('../database/database.php');

$pdo = connectToDb();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if ($_POST['password'] == $_POST['confirmpassword']) {

        $username = ($_POST['username']);
        $email = ($_POST['email']);
        $password = md5($_POST['password']);
        $avatar_path = ($_FILES['avatar']['name']);
        
        $sql = $pdo->prepare("INSERT INTO users (username, email, password, avatar) values (?,?,?,?)");

        $sql->bindParam($username,  $email, $password, $avatar_path);

        $sql->execute([$username,  $email, $password, $avatar_path]); 

        //make sure file typle is image
        if (preg_match("!image!", $_FILES['avatar']['type'])) {

            //copy image to images/ folder
            if (copy($_FILES['avatar']['tmp_name'], $avatar_path)) {

                $_SESSION['username'] = $username;
                $_SESSION['avatar'] = $avatar_path;

             

                    $_SESSION['message'] = "Registration succesful Added $username to the database ";
                    header("location: http://localhost:7000/welcome.php");
                
                
            } else {
                $_SESSION['message'] = "mamo";
            }
        } else {
            $_SESSION['message'] = "Pinche imagen de la verga";
        }
    } else {
        $_SESSION['message'] = "No te hagas pendejo no es la misma contraseña -.-";
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign</title>
</head>

<body>
    <div class="body-content">
        <div class="module">
            <h1>Create a account</h1>
            <form action="" class="form" method="POST" enctype="multipart/form-data" autocomplete="off">
                <div class="alert alert-erro"><?= $_SESSION['message'] ?></div>
                <input type="text" placeholder="Username" name="username" required>
                <input type="email" placeholder="Email" name="email" required>
                <input type="password" placeholder="Password" name="password" autocomplete="new-password" required>
                <input type="password" placeholder="Confirm Password" name="confirmpassword" autocomplete="new-password" required>
                <div class="avatar"><label for="">Select your avatar: </label><input type="file" name="avatar" accept="image/*" required></div>
                <input type="submit" value="Register" name="register" class="btn btn-block btn-primary">
               
            </form>
        </div>
    </div>
</body>

</html>