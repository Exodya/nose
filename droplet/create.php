<?php

$data = array("name" => "$droplet", "region" => "$region", "size" => "1gb", "image" => "ubuntu-18-04-x64");
$data_string = json_encode($data);
$ch = curl_init('https://api.digitalocean.com/v2/droplets');
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_FAILONERROR, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt(
    $ch,
    CURLOPT_HTTPHEADER,
    array(
        'Authorization: Bearer ba9f4826cb9a5b224f95edd3eee578e9df8e04763756108e91eba41617408d1a',
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string)
    )
);

$result = curl_exec($ch);

//var_dump(json_decode($result , true));

try {
    
    $json = json_decode($result, true);
    // var_dump($json);
   // echo $json["droplet"]["id"];
} catch (Exception $e) {
    print_r($e);
    die('chale :c');
}

curl_close($ch);

if (curl_errno($ch)) {
    echo curl_error($ch);
}
